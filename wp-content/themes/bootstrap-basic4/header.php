<?php

/**
 * The theme header.
 * 
 * @package bootstrap-basic4
 */

$container_class = apply_filters('bootstrap_basic4_container_class', 'container');
if (!is_scalar($container_class) || empty($container_class)) {
    $container_class = 'container';
}
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!--wordpress head-->
    <?php wp_head(); ?>
    <!--end wordpress head-->
</head>

<body <?php body_class(); ?> style="width: fit-content;">
    <div class="container-fluid page-container">
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
                <h1><a class="navbar-brand" href="#">Ai4</a></h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Agenda
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="https://ai4.io/schedule-of-events/">Schedule of Events</a>
                                <a class="dropdown-item" href="https://ai4.io/preliminary-agenda/#filter=.category-97">Premilinary Agenda</a>
                                <a class="dropdown-item" href="https://ai4.io/tracks-industry/">Industry Tracks</a>
                                <a class="dropdown-item" href="https://ai4.io/tracks-technical">Technical Tracks</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Speakers
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="https://ai4.io/speaker-lineup/">Speaker Lineup</a>
                                <a class="dropdown-item" href="https://ai4.io/application-speaker/">Apply To Speak</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Attendees
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="https://ai4.io/who-attends/">Who Attends</a>
                                <a class="dropdown-item" href="https://ai4.io/1-1-meetings/">1:1 Meetings</a>
                                <a class="dropdown-item" href="https://ai4.io/special-events/">Special Meetings</a>
                                <a class="dropdown-item" href="https://ai4.io/startups-2/">Startups</a>
                                <a class="dropdown-item" href="https://ai4.io/application-attendee-media">Media</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Sponsors
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="https://ai4.io/sponsors-2/">See Sponsors & Exhibitors</a>
                                <a class="dropdown-item" href="https://ai4.io/application-sponsor/">Become A Sponsor</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                About
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="https://ai4.io/our-story/">Our Story</a>
                                <a class="dropdown-item" href="https://ai4.io/venue/">Virtual Venue</a>
                                <a class="dropdown-item" href="https://ai4.io/faqs/">FAQs</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://ai4.io/training">AI Training</a>
                        </li>
                    </ul>
                    <div class="btn-Div redbtn">
                        <a href="https://ai4.io/application-attendee-2/" class="btn-blue">Apply For A Free Pass</a>
                    </div>
                    <div class="btn-Div bluebtn">
                        <a href="https://ai4.io/register" class="btn-red">Register Now</a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="content" class="site-content">