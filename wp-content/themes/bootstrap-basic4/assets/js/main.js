/**
 * Theme's main JS.
 *  
 * @license https://opensource.org/licenses/MIT MIT
 * @author Vee W.
 */

function afterHeaderLiClickToScroll() {
    // window.scroll(0,findPos(document.getElementById("after-header-top")));
    document.getElementById("after-header-top").scrollIntoView();
    window.scrollBy({
        top: -130,
        left: 0,
        behavior: 'smooth'
      });
}

// jQuery on DOM is ready. ------------------------------------------------------------------------------------------------
jQuery(document).ready(function($) {
    // mark no-js class as js because JS is working.
    $('html').removeClass('no-js').addClass('js');

    if($(this).scrollTop() == 0){
        $(".navbar").removeClass("header-bg-dark");
    }

    $(window).scroll(function() {
        if($(this).scrollTop() == 0){
           $(".navbar").removeClass("header-bg-dark");
        }
        else{
           $(".navbar").addClass("header-bg-dark");
        }
    });

    var liWidth = $('#content > section > div > ul > li:first-child').width()/2;
    var initArrowPos = $('#content > section > div > ul > li:first-child').position().left;
    // $('#content > section > div > ul > li:first-child').addClass('tabs-active');
    $('.tabs-widget ul li:first-child').addClass('tabs-active');
    $('.tabs-arrow').css({"left": (initArrowPos+liWidth+20)});

    $('.tabs-widget ul li').on('click', function() {debugger;
        // $('.tabs-widget ul li:first-child').removeClass('tabs-active');
        var liWidth = $(this).width()/2;
        var arrowPos = $(this).position().left;
        $('.tabs-arrow').css({"left": (arrowPos+liWidth+20)});
        var tabIndex = $(this).index();
        // $(this).addClass('tabs-active');
        // $('.tabs-widget ul li:nth-child('+(tabIndex-1)+')').removeClass('tabs-active');
        // $('.tabs-widget ul li:nth-child('+(tabIndex+1)+')').addClass('tabs-active');
        $('.tabs-widget ul li').removeClass('tabs-active');
        $('.tabs-widget ul li:nth-child('+(tabIndex+1)+')').addClass('tabs-active');

        $('.tabs-content').hide();
        $('.tabs-content:nth-child('+(tabIndex+1)+')').show();
    });

    $('#dataScienceToggle').on('click', function(e) {
        e.preventDefault();
        $('#dataScience').click();
    });

});
