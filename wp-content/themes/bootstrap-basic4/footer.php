<?php
/** 
 * The theme footer.
 * 
 * @package bootstrap-basic4
 */
?>
        <div class="container-fluid footer-section">
            <div class="">
                <div class="col-md-12 text-center" style="position: relative;">
                    <h1 class="saveDate">Did you save the date?</h1>
                    <p class="lead">
                        Ai4 2020: August 18-20, 1-5PM ET, Taking Place Digitally
                    </p>
                    <div style="padding-top: 0px; line-height: 0px; display: inline-block;">
                    <a href="https://add.eventable.com/events/5f129e20e9d83700ecc7aff9/5f129e26e9d83700ecc7affc" class="eventable-link" target="_blank" data-key="5f129e20e9d83700ecc7aff9" data-event="5f129e26e9d83700ecc7affc" data-style="3">Add to Calendar</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://plugins.eventable.com/eventable.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script', 'eventable-script');</script>
                        <!-- <iframe scrolling="no" style="border: none; padding: 0px; margin: 0px; overflow: hidden; height: 32px; width: 162px;"></iframe>
                        <div style="padding-top: 0px; line-height: 0px;">
                            <iframe scrolling="no" style="border: none; padding: 0px; margin: 0px; overflow: hidden; width: 169px; height: 205px; position: absolute; z-index: 9999; display: none;"></iframe>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
            <div class="">
                <div class="col-md-12 text-center">
                    <p><strong>Help Support Diversity &amp; Inclusion</strong>: Ai4 donates to <a href="https://www.blackgirlscode.com/" target="_blank">Black Girls Code</a>. We hope you'll consider donating too. </p>
                    <img src="https://1np04e3thcprxq5tj4az7bct-wpengine.netdna-ssl.com/wp-content/themes/ai4-custom/dist/images/BBC_Logo.png" style="height:50px; margin-bottom: 20px;">
                    <br />
                    <ul class="footer-menu align-center" data-dropdown-menu="" data-alignment="left" role="menubar" >
                        <li role="footer-menu-item">
                            <a href="mailto:jessica@ai4.io" class="footer-menu-item-a">Press Inquires</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="mailto:michelle@ai4.io" class="footer-menu-item-a">Sponsorship</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="/codeofconduct/" class="footer-menu-item-a">Code of Conduct</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="/privacypolicy" class="footer-menu-item-a">Privacy Policy</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="https://ai4.io/blog" class="footer-menu-item-a">Blog</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="https://ai4.io/digital-events/" class="footer-menu-item-a">Digital Events</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="mailto:jessica@ai4.io" class="footer-menu-item-a">Contact Us</a>
                        </li>
                    </ul>
                    <br />
                    <a href="https://twitter.com/Ai4Conferences" class="footer-menu-item-a twitter">
                        <img src="../wp-content/uploads/2020/07/icons8-twitter-svg.png" alt="twitter" />
                    </a>
                    <a href="https://www.linkedin.com/company/ai4/" class="footer-menu-item-a linkedin">
                        <img src="../wp-content/uploads/2020/07/icons8-linkedin-svg.png" alt="linkedin" />
                    </a>
                    <a href="https://www.facebook.com/Ai4.io/?ref=ai4" class="footer-menu-item-a facebook">
                        <img src="../wp-content/uploads/2020/07/icons8-facebook-svg.png" alt="facebook" />
                    </a>
                    <a href="https://www.instagram.com/ai4conferences/" class="footer-menu-item-a instagram">
                        <img src="../wp-content/uploads/2020/07/icons8-instagram-svg.png" alt="instagram" />
                    </a>
                    <a href="https://join.slack.com/t/ai4-workspace/shared_invite/zt-c4nedblf-GOyfzGLhfzNVeu9Vptm7YA" class="footer-menu-item-a slack">
                        <img src="../wp-content/uploads/2020/07/slack-symbol-svg-svg.png" alt="slack" />
                    </a>
                    <br />
                    <br />
                    <ul class="footer-menu" role="menubar" >
                        <li role="footer-menu-item">
                            <a href="/finance/" class="footer-menu-item-a">Ai4 Finance</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="/healthcare/" class="footer-menu-item-a">Ai4 Healthcare</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="/retail/" class="footer-menu-item-a">Ai4 Retail</a>
                        </li>
                        <li role="footer-menu-item">
                            <a href="/cybersecurity/" class="footer-menu-item-a">Ai4 Cybersecurity</a>
                        </li>
                    </ul>
                    <br />
                    <h1>#Ai4</h1>
                    <br />
                    <p class="copy">Copyright 2020 Ai4 LLC. All Rights Reserved.</p>
                </div>
            </div>
        </footer>
        </div><!--.site-content-->

        <!--wordpress footer-->
        <?php wp_footer(); ?> 
        <!--end wordpress footer-->
    </body>
</html>
