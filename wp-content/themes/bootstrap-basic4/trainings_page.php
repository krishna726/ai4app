<?php
 /* Template Name: CustomPage 
 * The page template file.<br>
 * This file works as display page content (post type "page") and its comments.
 * 
 * @package bootstrap-basic4
 */


// begins template. -------------------------------------------------------------------------
get_header();
// get_sidebar();
?> 

    <main id="main" class="col-md-12 site-main" role="main">
        <section class="page-header">
            <div class="container">
                <h1>Ai4</h1>
                <h3><?php echo get_the_title(); ?></h3>
            </div>
            <div class="gradient-divider"></div>
        </section>
        <?php
        if (have_posts()) {
            $Bsb4Design = new \BootstrapBasic4\Bsb4Design();
            while (have_posts()) {
                the_post();
                get_template_part('template-parts/content', 'page');
                echo "\n\n";

                $Bsb4Design->pagination();
                echo "\n\n";

                // If comments are open or we have at least one comment, load up the comment template
                if (comments_open() || '0' != get_comments_number()) {
                    comments_template();
                }
                echo "\n\n";
            }// endwhile;

            
            unset($Bsb4Design);
        } else {
            get_template_part('template-parts/section', 'no-results');
        }// endif;
        ?> 
    </main>
    <section id="after-header-top" class="after-header">
        <div class="tabs-widget text-center">
            <span class="tabs-arrow"></span>
            <ul><li>Executive Courses</li><li>Data Science Courses</li></ul>
        </div>
        <div class="container">
            <div class="tabs-content">
                <h4>See below for our 3-part AI training program for executives and managers! Designed and taught by <a href="https://bit.ly/2ZV0lc9" target="_blank" rel="noopener noreferrer">Flatiron School</a>, the three courses are part of a cohesive curriculum and designed to be taken in succession. You may purchase each course a-la-carte as well as opt out of either the beginner or even intermediate courses based on your own discretion.</h4>
                <br />
                <h5>Looking for our technical trainings for data scientists & engineers? <a id="dataScienceToggle" href="">Click here.</a></h5>
                <br />
                <img class="img-responsive" src="../wp-content/uploads/2020/07/Learning-Path-Execs-Color-Arrows.png" alt="Learning-Path-Execs-Color-Arrows"/>
                <br />
                <br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>august 3-4, 2020 | 1:00pm EDT | $275</span></h6>
                            <br />
                            <h3>AI For Executives: Machine Learning Fluency</h3>
                            <p>This introductory workshop will establish a baseline lexicon in machine learning that spans across industries. Upon completing the course, participants will be able to engage with their data science teams on a more substantive level — they will know how to ask the right questions and have the capacity to better understand the answers. Participants will learn various use cases for topic areas such as Machine Learning, Databases (SQL and NoSQL), and Artificial Intelligence. They will develop a deeper understanding of how machine learning teams clean, process, and visualize large data sets, as well as design and run A/B tests. By the end of this course, participants will gain best practices for building a data-driven organization and identifying key components that drive the success of machine learning projects.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Turquoise-Light-1.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="66"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 5-7, 2020 | 1:00PM EDT | $425</span></h6>
                            <br />
                            <h3>AI For Executives: A/B Tests to Machine Learning Pipelines</h3>
                            <p>This course builds on the baseline lexicon established in the beginner workshops and provides participants with the opportunity to create and run an actual statistical model: A/B Testing. Rather than just looking at slides on how machine learning works, participants will develop an understanding for these concepts by solving a real data-powered problem. Participants will complete their first A/B test and learn how to interpret the results, and how to know when the results are “too good to be true”. By experiencing this hands-on statistical project course participants will be able to ask better questions, not be tempted to peek at preemptive results, delve deeper into data analysis, and set far reaching goals for their teams.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Turquoise-Light-1.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="74"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 10-13, 2020 | 1:00PM EDT | $675</span></h6>
                            <br />
                            <h3>AI For Executives: ML Modeling with Industry Standard Tools</h3>
                            <p>The goal of this course is to help participants dive deeper into key concepts in machine learning, providing them the opportunity to explore data and answer questions with machine learning algorithms. Participants that complete this course will have a solid foundational understanding of Relational Databases, programming with Python, and practical experience with popular machine learning libraries and frameworks such as Pandas and Scikit-learn. Participants will be able to write effective database queries, automatically categorize customer data, and run A/B tests. This course will enable company leaders to answer questions with data and communicate them effectively to technical and nontechnical stakeholders in a clear, concise manner.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Turquoise-Light-1.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="78"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
            <div class="tabs-content" style="display:none">
                <h4>See below for our 6-part AI training program for data scientists & engineers! Each of the six courses are part of the same curriculum and designed to be taken in succession. You may purchase each course a-la-carte as well as opt out of earlier courses based on your own experience and discretion.</h4>
                <img class="img-responsive" src="../wp-content/uploads/2020/07/Learning-Path-Technical-Color-Arrows.png" alt="Learning-Path-Execs-Color-Arrows"/>
                <br />
                <br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>JULY 27-30, 2020 | 1:00PM EDT | $429</span></h6>
                            <br />
                            <h3>Python Basics</h3>
                            <p>Python has recently become the most popular language. It excels at data science, artificial intelligence, and other tasks but is also an outstanding language for web and service programming and general application development.</p>
                            <p>This course will help beginners to Python become comfortable with Language Basics and getting started with Python.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="80"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 3-6, 2020 | 1:00PM EDT | $429</span></h6>
                            <br />
                            <h3>Data Analytics with Python</h3>
                            <p>Python has become a powerful language and environment for performing data science.   It combines a robust, object-oriented language with a powerful library of data science packages, such as numpy, scipy, matlibplot, scikit-learn, and pandas.  These tools together make python one of the best combinations of robust programming language together with great library support.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="81"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 10-14, 2020 | 1:00PM EDT | $529</span></h6>
                            <br />
                            <h3>Machine Learning Essentials with Python</h3>
                            <p>Machine Learning (ML) is changing the world. To use ML effectively, one needs to understand the algorithms and how to utilize them. This course provides an introduction to the most popular machine learning algorithms.</p>
                            <p>This course teaches doing Machine Learning using the popular SciKit-Learn package in Python language.</p>
                            <p>This course teaches Machine Learning from a practical perspective. In-depth coverage of Math / Stats is beyond the scope of this course.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="82"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 24-28, 2020 | 1:00PM EDT | $529</span></h6>
                            <br />
                            <h3>Intro to Deep Learning With TensorFlow & Keras</h3>
                            <p>The abundance of data and affordable cloud scale has led to an explosion of interest in Deep Learning. Google has open sourced a library called TensorFlow which has become the de-facto standard, allowing state-of-the-art machine learning done at scale, complete with GPU-based acceleration.</p>
                            <p>This course introduces Deep Learning concepts and TensorFlow and Keras libraries to students.</p>
                            <p>This course teaches Machine Learning from a practical perspective. In-depth coverage of Math / Stats is beyond the scope of this course.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="83"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>AUGUST 31-SEPTEMBER 4, 2020 | 1:00PM EDT | $529</span></h6>
                            <br />
                            <h3>AI For Computer Vision & Image Analysis</h3>
                            <p>In the last few years, AI algorithms for image analysis have made tremendous progress. This is mainly due to the abundance of data, affordable computing, and exceptional libraries. Google has open-sourced a library called TensorFlow which has become the de facto standard, allowing the state of the art machine learning done at scale, complete with GPU based acceleration.</p>
                            <p>This course introduces Deep Learning concepts and TensorFlow and Keras libraries to students.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="84"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <br /><br /><br />
                <div clas="sub-post-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="sub-head"><span>SEPTEMBER 14-18, 2020 | 1:00PM EDT | $529</span></h6>
                            <br />
                            <h3>AI For Natural Language Processing</h3>
                            <p>We live in an era of so much data – a lot of it is text (emails, tweets, customer tickets, Yelp reviews, product reviews, etc.)</p>
                            <p>In the field of AI, there is a revolution going on in the past few years. The researchers from companies like Google, Facebook, Microsoft and Baidu has come up with breakthrough algorithms that can understand text data more than ever before.</p>
                            <p>The applications are wide-ranging, including understanding documents, processing customer service tickets and analyzing reviews.</p>
                            <p>In this course, we will teach how to handle text data and introduce you to modern AI NLP technologies.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../wp-content/uploads/2020/07/Tutorial-Icon-Blue-Light.png" class="img-responsive" alt="tutorial icon" />
                            <div class="btn-Div btnInside">
                                <a href="" class="btn-style">REGISTER NOW</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-8">
                            <?php echo do_shortcode('[sp_easyaccordion id="85"]'); ?>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tabs-widget text-center">
            <span class="tabs-arrow black-arrow"></span>
            <ul>
                <li onclick="afterHeaderLiClickToScroll()">Executive Courses</li>
                <li onclick="afterHeaderLiClickToScroll()" id="dataScience">Data Science Courses</li>
            </ul>
        </div>
    </section>
<?php
// get_sidebar('right');
get_footer();